import jdk.nashorn.internal.objects.annotations.Getter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ControllerClass {


    @GetMapping(value = "/greeting")
    public String handleGreeting(@RequestParam(name = "name", required = false, defaultValue = "World")
                                             String name, Model model){
        model.addAttribute("name", name);
        return "greeting";
    }
}
